var _ = underscore = require('underscore');
var userList = [];

var generateUserId = function() {
	return Math.round(Math.random() * 10000);	
};

var add = function( connId, gender ) {

	var user = {
		connId: connId,
		gender: gender,
		id: generateUserId(),
		available: true
	};	

	userList.push( user );

	return user;
};

var search = function( userId, connId ) {
	return _.find( userList, function( user ){
		return user.id == userId || user.connId == connId;	
	});	
};

var anyMale = function() {
	return anyUser( 'M' );	
};

var anyFemale = function() {
	return anyUser( 'F' );	
};

var inverseGender = function( gender ) {
	return gender == 'M' ? 'F' : 'M';	
};

var anyUser = function( gender ) {
	
	var c = count( gender );
	if ( ! c ) {
		return false;
	}	

	var user;
	do { user = _.sample( userList ); } while(user.gender != gender && user.available);

	return user;
};

var maleCount = function() {
	return count( 'M' );
};

var femaleCount = function() {
	return count( 'F' );	
};

var count = function( gender ) {
	
	if (typeof gender == 'undefined')
		return userList.length;

	else
		return _.filter( userList, function( user ){
			return user.gender == gender;	
		}).length;	
};

exports.add = add;
exports.search = search;
exports.anyMale = anyMale;
exports.anyFemale = anyFemale;
exports.count = count;
exports.maleCount = maleCount;
exports.femaleCount = femaleCount;
