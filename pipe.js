var conn = require('./conn');

// TODO: need to check if both sockets still active
var bridge = function( conn1, conn2 ) {

	conn1.on( 'data', function( dataStr ){
		
		var data = JSON.parse( dataStr );

		if (data.type == 'chat') {
			conn2.write(JSON.stringify({
				type: 'chat',
				val: data.val
			}));
		}

	});

};

var register = function( firstUser, secondUser ) {
	
	console.log( firstUser );
	console.log( secondUser );

	// TODO: need to check if both sockets still active
	var conn1 = conn.get( firstUser.connId ),
	    conn2 = conn.get( secondUser.connId );
	
	bridge( conn1, conn2 );
	bridge( conn2, conn1 );

	conn1.write(JSON.stringify({
		type: 'connected',
	}));

	conn2.write(JSON.stringify({
		type: 'connected',
	}));

	firstUser.available  = false;
	secondUser.available = false;
};

exports.register = register;
