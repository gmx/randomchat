var assert  = require('assert');

/*
var repoLib = require('./repo');
describe('pipe', function(){
	
	var firstUserId  = '123';
	var secondUserId = '234'
	
	var repo = repoLib.createRepo( firstUserId, secondUserId );

	it( 'should have pipe id', function(){
		// pipe id shouldn't be undefined
		assert.ok(typeof pipe.pipeId != 'undefined');
	});

	it( 'should be function', function(){
		// pipe id shouldn't be undefined
		assert.ok(pipe.constructor == Function);
	});

	pipe( firstUserId, 'Hello Buddy', 1000 );
	pipe( firstUserId, 'How are you getting on?', 1000 );
	pipe( secondUserId, 'Fine! and you?', 1000 );

	var conversation = pipeLib.getPipe( pipe.pipeId );

	it( 'userId check', function(){
		assert.ok(conversation.firstUserId == firstUserId);
		assert.ok(conversation.secondUserId == secondUserId);
	});

	it( 'message list should be array', function(){
		assert.ok(conversation.messages.constructor == Array);
	});
	
	it( 'message list check', function(){
		assert.ok(conversation.messages[0][0] == firstUserId);
		assert.ok(conversation.messages[1][0] == firstUserId);
		assert.ok(conversation.messages[2][0] == secondUserId);

		assert.ok(conversation.messages[0][1] == 'Hello Buddy');
		assert.ok(conversation.messages[1][1] == 'How are you getting on?');
		assert.ok(conversation.messages[2][1] == 'Fine! and you?');
	});
});
*/

var conn = require('./conn');

describe( 'connection', function(){
	
	it( 'check method existance', function(){
		assert.ok( conn.add.constructor == Function );		
		assert.ok( conn.get.constructor == Function );		

		assert.ok( conn.add.length == 1 );		
		assert.ok( conn.get.length == 1 );		
	});
	
	// mock connection object
	var conn1 = {
		id: '123-456-7890'		
	};

	var conn2 = {
		id: '234-567-8901'		
	};
	
	it( 'check behaviors', function(){
		conn.add( conn1 );
		conn.add( conn2 );
		assert.ok(conn.get( conn1.id ).id == conn1.id);
	});
});


var user = require('./user');

describe( 'user', function(){
	
	it( 'check method existance', function(){
		assert.ok( user.add.constructor == Function );
		assert.ok( user.add.length == 2 );

		assert.ok( user.search.constructor == Function );
		assert.ok( user.search.length == 2 );

		assert.ok( user.anyMale.constructor == Function );
		assert.ok( user.anyMale.length == 0 );

		assert.ok( user.anyFemale.constructor == Function );
		assert.ok( user.anyFemale.length == 0 );

		assert.ok( user.count.constructor == Function );
		assert.ok( user.count.length == 1 );

		assert.ok( user.maleCount.constructor == Function );
		assert.ok( user.maleCount.length == 0 );

		assert.ok( user.femaleCount.constructor == Function );
		assert.ok( user.femaleCount.length == 0 );
	});

	it( 'check behavior', function(){
		
		var conn1 = {
			id: '1234-5678-91011'		
		};

		var conn2 = {
			id: '2345-1122-12345'
		};

		var _user = user.add( conn1.id, 'M' );
		assert.ok(_user != null);
		assert.ok(_user != '');
		assert.ok(typeof _user != 'undefined');
		assert.ok(( _user.id + '' ).length == 4);

		var anotherUser = user.add( conn2.id, 'F' );
		assert.ok(_user.id != anotherUser.id);

		var foo = user.search( null, conn1.id );
		assert.ok(foo.id == _user.id);
		assert.ok(foo.connId == conn1.id);
		assert.ok(foo.gender == 'M');
		assert.ok(foo.available == true); // just inserted, so it should be 0

		var bar = user.search( _user.id, null );
		assert.ok(foo.id == bar.id);
	});
	
	it( 'check behavior for random matching', function(){
		var maleUser = user.anyMale();		
		assert.ok(maleUser.gender == 'M');
		assert.ok(maleUser.available == true);

		var femaleUser = user.anyFemale();
		assert.ok(femaleUser.available == true);
	});
});


