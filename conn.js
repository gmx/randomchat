var _ = underscore = require( 'underscore' );

var connList = [];

var get = function( connId ) {
	return _.find( connList, function( conn ){
		return conn.id == connId;
	});	
};

var add = function ( conn ) {
	connList.push( conn );
};

exports.get = get;
exports.add = add;
