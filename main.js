var http = require('http');
var sockjs = require('sockjs');
var express = require('express');
var app = express();
var fs = require('fs');
var _p = require('./pipe');

var user = require('./user');
var conn = require('./conn');

var echo = sockjs.createServer();

echo.on( 'connection', function( sock ) {
	
    sock.on( 'data', function( dataStr ) {

		var data = JSON.parse( dataStr );

		if (data.type == "connect") {
			console.log( data.gender );

			// save connection
			conn.add( sock );
			// put into the user list
			var _user = user.add( sock.id, data.gender );
			
			sock.write(JSON.stringify({
				type: 'setId',
				val: _user.id
			}));

			console.log( "UserId " + _user.id );

			// connect an available user of different gender
			var buddy = data.gender == 'M' ? user.anyFemale() : user.anyMale();
			console.log( "Buddy: " + buddy );
			// and make a pipe
			if ( buddy ) {
				_p.register( _user, buddy ); 
			} 

			else {
				// no active user
				// ask to wait
				// TODO: ask him or her to wait	
				sock.write(JSON.stringify({
					type: "wait"		
				}));
			}

		}

    });

    sock.on('close', function() {});

});

var server = http.createServer();
echo.installHandlers(server, {prefix:'/echo'});
server.listen(9999, '127.0.0.1');


app.get( '/', function(req, res) {
	var homepage = fs.readFileSync( 'main.html', 'utf8' );
	res.send( homepage );	
});

app.listen( 3000 );
